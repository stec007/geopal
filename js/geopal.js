// The map
var map = L.map('map', {drawControl: true}).setView([32,-6], 2.5);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution:'© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'}).addTo(map);

// Loads geojson file and shows points on map
function showPoints() {
    var pointlayer = new L.GeoJSON.AJAX(["data/upload.geojson"], {onEachFeature: addPopUps,
        pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, {
                radius: 10,
                fillColor: "#12d2bf",
                color: "#000",
                weight: 2,
                opacity: 1,
                fillOpacity: 0.9
            });
        }
    }).addTo(map);
    // Centre map on loaded points
    pointlayer.on('data:loaded', function() {
        map.fitBounds(pointlayer.getBounds())
    })
}

function addPopUps(feature, layer) {
    var elements = [];
    if (feature.properties) {
        for (key in feature.properties){
            elements.push(key + ": " + feature.properties[key]);
        }
        layer.bindPopup('<h3>Point</h3><div class="popup">' + elements.join('<br>') + '</div>');
    }

}

$(document).ready(function() {

    // File upload
    $('#upload-submit').on('click', function() {
        var formData = new FormData($('.form')[0]);
        $.ajax({
            url: "upload.php",
            type: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            success: function(data) {
                showPoints();
            }
        });
    });

    // Change colour
    $('#change-colour-submit').on('click', function() {
        map.eachLayer(function (layer) {
            if (layer.feature) {
                let colour = $('#colorpicker').val();
                layer.setStyle({fillColor: colour});
            }
        });
    });

    // Lasso
    $('#lasso').on('click', function () {
        $('#controls button').css({"cursor": "crosshair"});
        document.querySelector(".leaflet-draw-draw-rectangle").click();
    });

    // Drawing
    map.on(L.Draw.Event.CREATED, function (e) {
        let layer = e.layer;
        e.layer.setStyle({color: 'black', fillColor: 'gray', weight: 2, opacity: 1});
        var rectBounds = e.layer.getBounds();
        $('#controls button').css({"cursor": "default"});
        $('#selected-points-modal tbody').empty();
        console.log('Selected points CLEAR');
        map.eachLayer(function(layer) {
            if(layer instanceof L.Path) {
                if (layer.options.radius === 10) {
                    // Check if point within rectangle bounds (I'm sure there's a better way of doing this...)
                    if (layer._latlng.lat <= rectBounds._northEast.lat && layer._latlng.lat >= rectBounds._southWest.lat
                        && layer._latlng.lng <= rectBounds._northEast.lng && layer._latlng.lng >= rectBounds._southWest.lng) {
                        let address = layer.feature.properties.address;
                        if (!address) {
                            address = layer.feature.properties.ADDRESS;
                        }
                        $('#selected-points-modal tbody').append('<tr><td>' + address + '</td><td>' + layer._latlng.lat + '</td><td>' + layer._latlng.lng + '</td></tr>');
                    }
                }
            }
        });
        $('#selected-points-modal').modal('show');
    });

});


//  DRAWING (plugin)
// Initialise the FeatureGroup to store editable layers
var editableLayers = new L.FeatureGroup();
map.addLayer(editableLayers);

var drawPluginOptions = {
    position: 'topright',
    draw: {
        polygon: {
            allowIntersection: false, // Restricts shapes to simple polygons
            drawError: {
                color: '#e1e100', // Color the shape will turn when intersects
                message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
            },
            shapeOptions: {
                color: '#97009c'
            }
        },
        // disable toolbar item by setting it to false
        polyline: false,
        circle: false, // Turns off this drawing tool
        rectangle: false,
        marker: false,
    },
    edit: {
        featureGroup: editableLayers, //REQUIRED!!
        remove: false
    }
};

// Initialise the draw control and pass it the FeatureGroup of editable layers
var drawControl = new L.Control.Draw(drawPluginOptions);
map.addControl(drawControl);

var editableLayers = new L.FeatureGroup();
map.addLayer(editableLayers);

map.on('draw:created', function(e) {
    var type = e.layerType,
        layer = e.layer;

    if (type === 'marker') {
        layer.bindPopup('A popup!');
    }

    editableLayers.addLayer(layer);
});

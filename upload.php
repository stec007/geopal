<?php
// Simple file upload (no input validation...)
if (isset($_FILES['upload'])) {
	if (move_uploaded_file($_FILES['upload']['tmp_name'],'data/upload.geojson')) {
		die('File upload OK.');
	}
	die('Error uploading file.');
}